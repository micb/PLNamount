Function converts PLN numerical amount to words.

Main function files : plntowords.h and plntowords.c

Sample code how to use this function:
```
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "plntowords.h"

int
main(void)
{
    char     *result   = NULL; /* String with price */
    double    d_price  = 0;    /* Price in double format */
    uint64_t  ui_price = 0;    /* Price in unsigned int format */

    d_price  = 123456789.01;
    ui_price = (uint64_t) (d_price * 100.0);

    result = pln_amount_txt_double (d_price);

    printf ("amount : %.2f\n", d_price);
    printf ("result : %s\n\n", result);

    free (result);

    result = pln_amount_txt (ui_price);

    printf ("amount : %.2f\n", d_price);
    printf ("result : %s\n\n", result);

    free (result);

    return 0;
}
```

This code can be found in file amount_example.c

Example price: *123456789.01*

Result: *sto dwadzieścia trzy miliony czterysta pięćdziesiąt sześć tysięcy siedemset osiemdziesiąt dziewięć złotych 1/100*

My email in case of problems: michal.babik@protonmail.com

Copyright (C) 2018-2020 Michał Bąbik

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

