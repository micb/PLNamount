# -*- coding: UTF-8 -*-
#
# @file plntowords.c
# @copyright Copyright (C) 2009-2020 Michał Bąbik
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# @brief  PLN numerical amount in words
#
# @date December 11, 2020
#
# @author Michal Babik <michal.babik@protonmail.com>
#-----------------------------------------------------------------------------#
def pln_words(price):
    hundreds = {0: "", 1: "sto ", 2: "dwieście ", 3: "trzysta ",
             4: "czterysta ", 5: "pięćset ", 6: "sześćset ",
             7: "siedemset ", 8: "osiemset ", 9: "dziewięćset "}
    tee = {0: "dziesięć ", 1: "jedenaście ", 2: "dwanaście ",
              3: "trzynaście ", 4: "czternaście ", 5: "piętnaście ",
              6: "szesnaście ", 7: "siedemnaście ", 8: "osiemnaście ",
              9: "dziewiętnaście "}
    tens = {0: "", 2: "dwadzieścia ", 3: "trzydzieści ",
                  4: "czterdzieści ", 5: "pięćdziesiąt ",
                  6: "sześćdziesiąt ", 7: "siedemdziesiąt ",
                  8: "osiemdziesiąt ", 9: "dziewięćdziesiąt "}
    ones = {1: "jeden ", 2: "dwa ", 3: "trzy ", 4: "cztery ", 5: "pięć ",
            6: "sześć ", 7: "siedem ", 8: "osiem ", 9: "dziewięć "}
    ends = (("złoty ", "złote ", "złotych ",),
            ("tysiąc ", "tysiące ", "tysięcy ",),
            ("milion ", "miliony ", "milionów ",),
            ("miliard ", "miliardy ", "miliardów ",),
            ("bilion ", "biliony ", "bilionów ",),
            ("biliard", "biliardy", "biliardów",),
            ("trylion", "tryliony", "trylionów",),
            ("tryliard", "tryliardy", "tryliardów",),
            ("kwadrylion", "kwadryliony", "kwadrylionów",),
            ("kwintylion", "kwintyliony", "kwintylionów",),
            ("sekstylion", "sekstyliony", "sekstylionów",),)
    number = price
    pricef = "{0:.2f}".format(number)
    lennorest = len(pricef) - 3
    while lennorest % 3:
        pricef = "0" + pricef
        lennorest = len(pricef) - 3
    rest = pricef[lennorest + 1:]
    result = rest + "/100"
    LBR = pricef[:lennorest]
    LBR0 = LBR
    for i in range(lennorest//3):
        LBR1 = LBR0[-3:]
        LBR0 = LBR0[:-3]
        L1, L2, L3 = [int(k) for k in LBR1]
        if L2 == 1: result = tee[L3] + ends[i][2] + result
        else:
            if L3 == 0:
                if i == 0:
                    result = ends[i][2] + result
                    if L1 == 0 and L2 == 0 and lennorest == 3:
                        result = "zero " + result
                elif not (L1 == 0 and L2 == 0 and lennorest > ((i + 1) * 3)):
                    result = ends[i][2] + result
            elif L3 == 1:
                if L1 == 0 and L2 == 0: result = ones[L3] + ends[i][0] + result
                else: result = ones[L3] + ends[i][2] + result
            elif L3 > 1 and L3 < 5: result = ones[L3] + ends[i][1] + result
            else: result = ones[L3] + ends[i][2] + result
            result = tens[L2] + result
        result = hundreds[L1] + result
    return result
#-----------------------------------------------------------------------------#

 
