/** 
 * @file amount_example.c
 * @copyright Copyright (C) 2018-2020 Michał Bąbik
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @brief  Example use of converting PLN numerical amount to words
 *
 * @date July 10, 2020
 *
 * @version 1.7
 *
 * @author Michal Babik <michal.babik@protonmail.com>
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "plntowords.h"
/*----------------------------------------------------------------------------*/
int
main (void)
{
    char     *result   = NULL; /* String with price */
    double    d_price  = 0;    /* Price in double format */
    uint64_t  ui_price = 0;    /* Price in unsigned int format */

    d_price = 123456789.01;
    ui_price = (uint64_t) (d_price * 100.0);

    result = pln_amount_txt_double (d_price);
    printf ("amount : %.2f\n", d_price);
    printf ("result : %s\n\n", result);
    free (result);

    result = pln_amount_txt (ui_price);
    printf ("amount : %.2f\n", (double) ui_price / 100.0);
    printf ("result : %s\n\n", result);
    free (result);

    d_price = 10.20;
    result = pln_amount_txt_double (d_price);
    printf ("amount : %.2f\n", d_price);
    printf ("result : %s\n\n", result);
    free (result);

    d_price = 255.43;
    result = pln_amount_txt_double (d_price);
    printf ("amount : %.2f\n", d_price);
    printf ("result : %s\n\n", result);
    free (result);

    d_price = 253.33;
    result = pln_amount_txt_double (d_price);
    printf ("amount : %.2f\n", d_price);
    printf ("result : %s\n\n", result);
    free (result);

    d_price = 1301.21;
    result = pln_amount_txt_double (d_price);
    printf ("amount : %.2f\n", d_price);
    printf ("result : %s\n\n", result);
    free (result);

    d_price = 51609.34;
    result = pln_amount_txt_double (d_price);
    printf ("amount : %.2f\n", d_price);
    printf ("result : %s\n\n", result);
    free (result);

    result = pln_amount_txt (5160934);
    printf ("amount : %.2f\n", d_price);
    printf ("result : %s\n\n", result);
    free (result);

    d_price = 268106.00;
    result = pln_amount_txt_double (d_price);
    printf ("amount : %.2f\n", d_price);
    printf ("result : %s\n\n", result);
    free (result);

    d_price = 2040107.01;
    result = pln_amount_txt_double (d_price);
    printf ("amount : %.2f\n", d_price);
    printf ("result : %s\n\n", result);
    free (result);

    d_price = 49100402.10;
    result = pln_amount_txt_double (d_price);
    printf ("amount : %.2f\n", d_price);
    printf ("result : %s\n\n", result);
    free (result);

    d_price = 701589401.50;
    result = pln_amount_txt_double (d_price);
    printf ("amount : %.2f\n", d_price);
    printf ("result : %s\n\n", result);
    free (result);

    d_price = 9018281042.00;
    result = pln_amount_txt_double (d_price);
    printf ("amount : %.2f\n", d_price);
    printf ("result : %s\n\n", result);
    free (result);
    return 0;
}
/*----------------------------------------------------------------------------*/

