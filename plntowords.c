/**
 * @file plntowords.c
 * @copyright Copyright (C) 2018-2020 Michał Bąbik
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @brief  PLN numerical amount in words
 *
 * @date July 10, 2020
 *
 * @version 1.7
 *
 * @author Michal Babik <michal.babik@protonmail.com>
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include "plntowords.h"
/*----------------------------------------------------------------------------*/
static const char
*hundreds[] = {
    "", "sto ", "dwieście ", "trzysta ", "czterysta ", "pięćset ",
    "sześćset ", "siedemset ", "osiemset ", "dziewięćset "};
static const char
*teens[] = {
    "dziesięć ", "jedenaście ", "dwanaście ", "trzynaście ",
    "czternaście ", "piętnaście ", "szesnaście ", "siedemnaście ",
    "osiemnaście ", "dziewiętnaście "};
static const char
*tens[] = {
    "", "", "dwadzieścia ", "trzydzieści ", "czterdzieści ",
    "pięćdziesiąt ", "sześćdziesiąt ", "siedemdziesiąt ",
    "osiemdziesiąt ", "dziewięćdziesiąt "};
static const char
*ones[] = {
    "", "jeden ", "dwa ", "trzy ", "cztery ", "pięć ",
    "sześć ", "siedem ", "osiem ", "dziewięć "};
static const char
*ends[11][3] = {
    {"złoty ",     "złote ",      "złotych "},
    {"tysiąc ",    "tysiące ",    "tysięcy "},
    {"milion ",    "miliony ",    "milionów "},
    {"miliard ",   "miliardy ",   "miliardów "},
    {"bilion ",    "biliony ",    "bilionów "},
    {"biliard",    "biliardy",    "biliardów"},
    {"trylion",    "tryliony",    "trylionów"},
    {"tryliard",   "tryliardy",   "tryliardów"},
    {"kwadrylion", "kwadryliony", "kwadrylionów"},
    {"kwintylion", "kwintyliony", "kwintylionów"},
    {"sekstylion", "sekstyliony", "sekstylionów"}};
/*----------------------------------------------------------------------------*/
/**
 * @fn          static uint64_t power (uint64_t x, int n)
 * @brief       Calculate power of x.
 * @param[in] x Base value
 * @param[in] n Exponent value
 * @return      Base x raised to the power n
 *
 * @fn            static uint8_t get_digit (uint64_t x, uint8_t pos)
 * @brief         Get digit of a number at given pos.
 * @param[in] x   Number to get digit from
 * @param[in] pos Position of digit to get
 * @return    Digit at position pos
 *
 * @fn           static void divide_split (uint64_t *x, uint8_t *y)
 * @brief        Get 3 lowest digits and divide price by 1000.
 * @param[in]  x Pointer to a price
 * @param[out] y Pointer to 3 items array for digits
 * @return     none
 *
 * @fn             static void check_resize (char **buff, const size_t ui_len)
 * @brief          Check if buffer is null and allocates memory or
 *                 reallocate if it is not null.
 * @param[in,out]  buff    Buffer to check.
 * @param[in]      ui_len  Length of a new buffer.
 * @return         none
 *
 * @fn                static int prestr (uint8_t n, ...)
 * @brief             Prepend strings to a destination string.
 *                    String will be expanded to proper size.
 * @param[in]     n   Number of all strings (src and dest)
 * @param[in,out] ... First one is a destination string and later ones are
 *                    strings to prepend before the destination one
 * @return        Return value
 */
/*----------------------------------------------------------------------------*/
static uint64_t power        (uint64_t       x,
                              int            n)   __attribute__ ((const));

static uint8_t  get_digit    (uint64_t       x,
                              uint8_t        pos) __attribute__ ((const));

static void     divide_split (uint64_t      *x,
                              uint8_t       *y);

static void     check_resize (char         **buff,
                              const size_t   ui_len);

static int      prestr       (uint8_t        n,
                                             ...);
/*----------------------------------------------------------------------------*/
/**
 * @brief  Calculate power of x.
 */
static uint64_t
power (uint64_t x,
       int      n)
{
    return n == 0 ? 1 : x * power (x, n - 1);
}
/*----------------------------------------------------------------------------*/
/**
 * @brief  Get digit of a number at given pos.
 */
static uint8_t
get_digit (uint64_t x,
           uint8_t  pos)
{
    return (uint8_t) ((x - x / power (10, pos + 1) * power (10, pos + 1)) / 
            power (10, pos));
}
/*----------------------------------------------------------------------------*/
/**
 * @brief  Get 3 lowest digits and divide price by 1000.
 */
static void
divide_split (uint64_t *x,
              uint8_t  *y)
{
    y[2] = get_digit (*x, 0);
    y[1] = get_digit (*x, 1);
    y[0] = get_digit (*x, 2);
    *x /= 1000;
}
/*----------------------------------------------------------------------------*/
/**
 * @brief  Check if buffer is null and allocates memory or reallocate if it
 *         is not null.
 */
static void
check_resize (char         **buff,
              const size_t   ui_len)
{
    if (*buff == NULL) {
        *buff = malloc (ui_len * sizeof (char));
    }
    else {
        *buff = realloc (*buff, ui_len * sizeof (char));
    }
    if (*buff == NULL) {
        puts ("Error (re)allocating memory");
        exit (EXIT_FAILURE);
    }
}
/*----------------------------------------------------------------------------*/
/**
 * @brief  Prepend strings to a destination string.
 */
static int
prestr (uint8_t n, ...)
{
    char    **dst    = NULL; /* Pointer to destination string */
    char     *src    = NULL; /* Source strings to prepend */
    uint8_t   i      = 0;    /* i */
    size_t    ui_srl = 0;    /* Length of source string */
    size_t    ui_dsl = 0;    /* Length of destination string */
    va_list   vl;            /* Information about variable arguments */

    va_start (vl, n);
    dst = va_arg (vl, char**);
    for (i = 1; i < n; ++i) {
        src = va_arg (vl, char*);
        ui_srl = strlen (src);
        ui_dsl = strlen (*dst);
        check_resize (dst, ui_srl + ui_dsl + 1);
        memmove ((*dst) + ui_srl, *dst, ui_dsl + 1);
        memmove (*dst, src, ui_srl);
    }
    return 0;
}
/*----------------------------------------------------------------------------*/
/**
 * @brief  Function converts price (in PLN) given as a floating point number to
 *         a word version.
 */
char *
pln_amount_txt_double (double d_price)
{
    double d_val = 0;

    d_val = d_price * 100.0;
    d_val = round (d_val);

    return pln_amount_txt ((uint64_t) d_val );
}
/*----------------------------------------------------------------------------*/
/**
 * @brief  Function converts price (in PLN) given as an integer number to a
 *         word version.
 */
char *
pln_amount_txt (uint64_t ui_inprice)
{
    char     *s_price  = NULL;      /* Result string with price */
    uint64_t  ui_price = 0;         /* Price without "gr" after comma */
    uint64_t  ui_gr    = 0;         /* After comma "gr" rest */
    uint64_t  ui_prtmp = 0;         /* Temp price to count length */
    uint8_t   ui_prlen = 0;         /* Length of price in 3 char portions */
    uint8_t   p [3]    = {0, 0, 0}; /* 3 chars of a price portion */
    uint8_t   i        = 0;         /* i */

    ui_price = ui_inprice / 100;
    ui_prtmp = ui_price;
    ui_gr    = ui_inprice - (ui_inprice / 100 * 100);

    while (ui_prtmp || ui_prlen % 3) {
        ++ui_prlen;
        ui_prtmp /= 10;
    }
    check_resize (&s_price, 10);
    sprintf (s_price, "%" PRIu64 "/100", ui_gr);
    for (i = 0; i < ui_prlen / 3; ++i) {
        divide_split (&ui_price, p);
        if (p[1] == 1) prestr (3, &s_price, ends[i][2], teens[p[2]]);
        else {
            if (p[2] == 0) {
                if (i == 0) {
                    prestr (2, &s_price, ends[i][2]);
                    if (p[0] == 0 && p[1] == 0 && ui_prlen == 3)
                        prestr (2, &s_price, "zero ");
                }
                else if (! (p[0] == 0 && p[1] == 0 && ui_prlen > ((i + 1) * 3)))
                    prestr (2, &s_price, ends[i][2]);
            }
            else if (p[2] == 1) {
                if (p[0] == 0 && p[1] == 0)
                    prestr (3, &s_price, ends[i][0], ones[p[2]]);
                else prestr (3, &s_price, ends[i][2], ones[p[2]]);
            }
            else if (p[2] > 1 && p[2] < 5)
                prestr (3, &s_price, ends[i][1], ones[p[2]]);
            else prestr (3, &s_price, ends[i][2], ones[p[2]]);
            prestr (2, &s_price, tens[p[1]]);
        }
        prestr (2, &s_price, hundreds[p[0]]);
    }
    return s_price;
}
/*----------------------------------------------------------------------------*/

