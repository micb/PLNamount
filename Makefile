# Copyright (C) 2019-2020 Michał Bąbik
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
CC = gcc
CFLAGS = -march=native -O2 -std=gnu17
LDFLAGS = 
LIBS = -lm
PACKAGE = 
LDLIBS = 
SRCS = amount_example.c plntowords.c
OBJS = $(SRCS:.c=.o)
MAIN = example

.PHONY: all clean

all: $(MAIN)

$(MAIN): $(OBJS)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $(MAIN) $(OBJS) $(LIBS)

.c.o:
	$(CC) $(CFLAGS) $(PACKAGE) -c $<  -o $@

clean:
	$(RM) *.o *~ $(MAIN)

