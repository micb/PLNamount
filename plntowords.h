/**
 * @file plntowords.h
 * @copyright Copyright (C) 2018-2020 Michał Bąbik
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @brief  PLN numerical amount in words
 *
 * @date July 10, 2020
 *
 * @version 1.7
 *
 * @author Michal Babik <michal.babik@protonmail.com>
 */
#ifndef PLNTOWORDS_H
#define PLNTOWORDS_H
#include <inttypes.h>
/*----------------------------------------------------------------------------*/
/**
 * @fn  char * pln_amount_txt (uint64_t ui_inprice)
 * @brief      Function converts price (in PLN) given as an integer number to
 *             a word version.
 * @param[in]  ui_inprice  Price in integer format. It should be multiplied by
 *                         100 eg. price 100.00 should be passed as 10000
 * @return     Newly created string with price in words. It should be freed
 *             after use.
 *
 * @fn  char * pln_amount_txt_double (double d_price)
 * @brief      Function converts price (in PLN) given as a floating point
 *             number to a word version.
 * @param[in]  d_price  Price in double format eg. 100.00
 * @return     Newly created string with price in words. It should be freed
 *             after use.
 */
/*----------------------------------------------------------------------------*/
char * pln_amount_txt        (uint64_t ui_inprice);

char * pln_amount_txt_double (double   d_price);
/*----------------------------------------------------------------------------*/
#endif

